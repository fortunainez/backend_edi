<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $guarded = [];
    public function parent()
    {
        return $this->belongsTo('App\Models\User', 'employee_id', 'id');
    }
}
