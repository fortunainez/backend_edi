<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Employee;
use Auth;

class UsersController extends Controller
{
    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            if($user->status == 1){
                return response()->json([
                    'success' => true,
                    'token' => $user->createToken('appToken')->accessToken,
                    'user' => $user,
                ]);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'User Inactive, please contact admin!',
                ], 401);
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }
    }
    public function me()
    {
        $user = Auth::guard('api')->user();
        $user->greeting = "Selamat Datang";
        $user->employee = $user->employee;
        return response()->json([
            'success' => true,
            'data' => $user,
        ]);
        
    }
    public function userlist()
    {
        $user_data = Auth::guard('api')->user();
        if($user_data->role == 'admin'){
            $user = User::all();
        }else{
            $user = User::where('id',$user_data->id)->get();
        }
        return response()->json([
            'status' => 200,
            'data' => $user,
            'message' => '',
            'errors' => ''
        ]);
    }
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),
            ], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['role'] = 'user';
        $input['status'] = 0;
        $user = User::create($input);
        $success['token'] = $user->createToken('appToken')->accessToken;
        return response()->json([
            'success' => true,
            'token' => $success,
            'user' => $user
        ]);
    }
    public function updateemployee(Request $request)
    {
        $user_data = Auth::guard('api')->user();
        $user_id = $user_data->id;
        // dd($user_id);

        if ($user_data->employee_id != null) {
            $employee_id = $user_data->employee_id;
            $employee = Employee::find($employee_id);
            $employee->position = $request->position;
            $employee->name = $request->name;
            $employee->identity_no = $request->identity_no;
            $employee->place_of_birth = $request->place_of_birth;
            $employee->gender = $request->gender;
            $employee->religion = $request->religion;
            $employee->blood_type = $request->blood_type;
            $employee->status = $request->status;
            $employee->identity_address = $request->identity_address;
            $employee->address = $request->address;
            $employee->email = $request->email; 
            $employee->phone = $request->phone;
            $employee->reference_number = $request->reference_number;
            $employee->skill = $request->skill;
            $employee->placement = $request->placement;
            $employee->expected_salary = $request->expected_salary;
            $update = $employee->save();
            if($update){
                return response()->json([
                    'success' => true,
                    'message' => 'Update successfully!',
                ]);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'Update failed!',
                ]);
                
            }
           
        } else {
            $employee = Employee::create([
                'position' => $request->position,
                'name' => $request->name,
                'identity_no' => $request->identity_no,
                'place_of_birth' => $request->place_of_birth,
                'gender' => $request->gender,
                'religion' => $request->religion,
                'blood_type' => $request->blood_type,
                'status' => $request->status,
                'identity_address' => $request->identity_address,
                'address' => $request->address,
                'email' => $request->email,
                'phone' => $request->phone,
                'reference_number' => $request->reference_number,
                'skill' => $request->skill,
                'placement' => $request->placement,
                'expected_salary' => $request->expected_salary,
            ]);
            if($user_data->role != 'admin') {
                $user = User::find($user_data->id);
                $user->employee_id = $employee->id;
                $user->save();
            }
            if ($employee) {
                return response()->json([
                    'success' => true,
                    'message' => 'Add data employee successfully!',
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Add data employee failed!',
                ]);
            }
           
        }
    }
    public function employeelist()
    {
        $user_data = Auth::guard('api')->user();
        if ($user_data->role == 'admin') {
            $employee = Employee::all();
        } else {
            $employee = Employee::where('id', $user_data->employee_id)->get();
        }
        return response()->json([
            'status' => 200,
            'data' => $employee,
            'message' => '',
            'errors' => ''
        ]);
    }
    public function updatestatus(Request $request)
    {
        $user_id = $request->user_id;
        $user = User::find($user_id);
        $user->status = $request->status_active;
        $save_data = $user->save();
        if ($save_data) {
            return response()->json([
                'success' => true,
                'message' => 'Update status successfully!',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Update status failed!',
            ]);
        }

    }
    public function deleteemployee(Request $request)
    {
        try {
            $id = $request->id;
            $user = User::where('employee_id', $id)->first();
            if ($user){
                $user->employee_id = NULL;
                $user->save();
            }
            $employee = Employee::find($id);
            $employee->delete();
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json([
                'status'     => false,
                'message'     => 'Error delete data'
            ], 400);
        }
        return response()->json([
            'status'     => true,
            'message' => 'Success delete data'
        ], 200);
    }
    public function logout(Request $request)
    {
        if (Auth::user()) {
            $user = Auth::user()->token();
            $user->revoke();
            return response()->json([
                'success' => true,
                'message' => 'Logout successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Unable to Logout',
            ]);
        }
    }
}
