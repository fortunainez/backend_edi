<?php

namespace App\Http\Controllers;

use App\EmployeeEmploymentHistory;
use Illuminate\Http\Request;

class EmployeeEmploymentHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeEmploymentHistory  $employeeEmploymentHistory
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeEmploymentHistory $employeeEmploymentHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeEmploymentHistory  $employeeEmploymentHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeEmploymentHistory $employeeEmploymentHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeEmploymentHistory  $employeeEmploymentHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeEmploymentHistory $employeeEmploymentHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeEmploymentHistory  $employeeEmploymentHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeEmploymentHistory $employeeEmploymentHistory)
    {
        //
    }
}
