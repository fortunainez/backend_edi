<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('position');
            $table->string('name');
            $table->string('identity_no');
            $table->string('place_of_birth');
            $table->string('gender');
            $table->string('religion');
            $table->string('blood_type');
            $table->string('status');
            $table->text('identity_address');
            $table->text('address');
            $table->string('email');
            $table->string('phone');
            $table->string('reference_number');
            $table->string('skill');
            $table->string('placement');
            $table->double('expected_salary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
