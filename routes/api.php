<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::post('login', 'UsersController@login');
    Route::post('register', 'UsersController@register');
    Route::get('me', 'UsersController@me');
    Route::get('userlist', 'UsersController@userlist');
    Route::get('employee/list', 'UsersController@employeelist');
    Route::post('employee/update', 'UsersController@updateemployee');
    Route::post('user/update', 'UsersController@updatestatus');
    Route::post('/employee/delete', 'UsersController@deleteemployee');
    Route::get('logout', 'UsersController@logout')->middleware('auth:api');
});
